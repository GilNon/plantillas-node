const docxTemplate = require('docx-templates');
const fs = require('fs').promises;
const libreOffice = require('libreoffice-convert');

const data = {
  nombreAlumno: 'Gilberto',
  apellidoPaterno: 'Vazquez',
  apellidoMaterno: 'Noriega',
  numeroControl: '14190342',
  sexoAlumno: 'Masculino',
  carrera: 'Ing. Sistemas',
  plan: 'XXXXXXXXX',
};

const genDocument = async () => {
  //generar el documento con los nuevos datos
  const template = `${__dirname}/plantilla/plantilla.docx`;
  const docxOutput = `${__dirname}/documento-salida/salida.docx`;

  await docxTemplate({
    template,
    output: docxOutput,
    data,
    cmdDelimiter: ['{', '}'],
  });

  //convertir el DOCX generado a PDF
  const namePDF = 'salida';
  const extens = '.pdf';
  const outputPDF = `${__dirname}/documento-salida/${namePDF}${extens}`;

  const docx = await fs.readFile(docxOutput);

  libreOffice.convert(docx, extens, undefined, async (err, done) => {
    if (err) {
      console.log(`Error al convertir el archivo: ${err}`);
    }
    await fs.writeFile(outputPDF, done);
  });

  //eliminar el archivo DOCX y solo quedarnos con el PDF
  await fs.unlink(docxOutput);
};

genDocument();
